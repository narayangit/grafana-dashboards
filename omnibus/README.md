# GitLab Omnibus Dashboards

The Grafana dashbaord json files in this directory are tailored to the GitLab Omnibus install.

## [Overview Dashboard](overview.json)

This dashboard provides a basic overview of the health of a GitLab Omnibus intall.

It can also be found on [grafana.com](https://grafana.com/dashboards/5774).
